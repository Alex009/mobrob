import rospy
from std_msgs.msg import Float64
from sensor_msgs.msg import JointState

class PendulumController:
def __init__(self):
rospy.init_node('pendulum_controller')
self.pub = rospy.Publisher('/pendulum_joint/command', Float64, queue_size=10)
self.sub = rospy.Subscriber('/joint_states', JointState, self.callback) #подписка для получения x и v
# настройка коэф ПД регулятора
self.Kp = 1.0 
self.Kd = 0.1

self.prev_error = 0.0
self.prev_time = rospy.Time.now()

#ДОБАВИТЬ ВВОД ПОЛЬЗОВАТЕЛЕМ НАЧАЛЬНОГО УСКОРЕНИЯ/ВОЗДЕЙСТВИЯ НА МАШИНКУ    

def callback(self, data): #метод вычисления ошибки
# Получаем текущие q и q'
q = data.position[0]
q_dot = data.velocity[0]

# Рассчитываем ошибку между текущим углом и желаемым углом (нулем)
error = -q

# Вычисляем производную ошибки
dt = (rospy.Time.now() - self.prev_time).to_sec()
if dt == 0:
return
error_dot = (error - self.prev_error) / dt

# Вычисляем управляющий вход с помощью ПД регулятора
u = self.Kp * error + self.Kd * error_dot

# Публикация управляющего ввода в соединение
self.pub.publish(u)

# Сохранение текущей ошибки и времени для следующей итерации
self.prev_error = error
self.prev_time = rospy.Time.now()

if __name__ == '__main__':
try:
controller = PendulumController()
rospy.spin()
except rospy.ROSInterruptException:
pass 