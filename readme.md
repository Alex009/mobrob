# Перевернутый маятник

В этом проекте представлен мобильный робот с закрепленным на нем маятником.
Задача пользователя состоит в том, чтобы, подобрав скорости вращения колес, установить маятник вертикально вверх.

## Инструкция по запуску

1. Установите ROS на вашу систему (пропустите шаг, если ROS установлен):

`sudo apt-get install ros-melodic-desktop-full`

2. Создайте и настройте рабочее пространство ROS:

`mkdir -p catkin_ws/src`
`cd catkin_ws/src `

3. Склонируйте репозиторий в папку catkin_ws/src:

`git clone https://gitlab.com/Alex009/mobrob.git`

4. В другом терминале запустите ROS ядро:

`roscore`

5. Вернитесь в первый термина и запустите модель:
 
 `cd mobrob/launch`
 `roslaunch gazebo.launch`

## Исполнение

>Изменять скорость движения колес можно этими командами отдельно для каждого колеса:
`rostopic pub /controller_w1/command std_msgs/Float64 "data: var" ` 
`rostopic pub /controller_w2/command std_msgs/Float64 "data: var"`
`rostopic pub /controller_w3/command std_msgs/Float64 "data: var"`
`rostopic pub /controller_w4/command std_msgs/Float64 "data: var"`

>var = 0.0 ... 10.0 ... n - играйтесь ;)

Наблюдать изменение положения маятника можно непострественно в gazebo или получать обратную связть:

 `rostopic echo /joint_states`

 ## Перечень основных файлов в проекте

 -- папка config - содержит файл с перечислением котролеров;
 -- папка launch - содержит файлы запуска модели;
 -- папка meshes - содержит STL модели деталей мобильного робота;
 -- папка urdf - содержит файлы с описание структуры и связей в роботе;
 
 ## Автор

  Alex009
  https://gitlab.com/Alex009
